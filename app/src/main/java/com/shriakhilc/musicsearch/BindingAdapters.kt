package com.shriakhilc.musicsearch

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.core.net.toUri
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.shriakhilc.musicsearch.adapters.MusicSearchAdapter
import com.shriakhilc.musicsearch.network.MusicTrack
import com.shriakhilc.musicsearch.viewmodels.SearchQueryStatus

@BindingAdapter("listData")
fun bindRecyclerView(recyclerView: RecyclerView, data: List<MusicTrack>?) {
    val adapter = recyclerView.adapter as MusicSearchAdapter
    adapter.submitList(data)
}

@BindingAdapter("imageUrl")
fun bindImage(imgView: ImageView, imgUrl: String?) {
    imgUrl?.let {
        val imgUri = imgUrl.toUri().buildUpon().scheme("https").build()
        imgView.load(imgUri) {
            placeholder(R.drawable.loading_animation)
            error(R.drawable.ic_broken_image)
        }
    }
}

@BindingAdapter("recyclerStatus")
fun bindRecyclerStatus(recyclerView: RecyclerView, status: SearchQueryStatus?) {
    when (status) {
        SearchQueryStatus.LOADING, SearchQueryStatus.ERROR, SearchQueryStatus.EMPTY -> {
            recyclerView.visibility = View.GONE
        }
        SearchQueryStatus.DONE -> {
            recyclerView.visibility = View.VISIBLE
        }
        else -> {
            recyclerView.visibility = View.GONE
        }
    }
}

@BindingAdapter("searchStatus")
fun bindTextStatus(statusTextView: TextView, status: SearchQueryStatus?) {
    when (status) {
        SearchQueryStatus.LOADING -> {
            statusTextView.visibility = View.VISIBLE
            statusTextView.text = statusTextView.context.getString(R.string.status_loading)
        }
        SearchQueryStatus.ERROR -> {
            statusTextView.visibility = View.VISIBLE
            statusTextView.text = statusTextView.context.getString(R.string.status_error)
        }
        SearchQueryStatus.DONE -> {
            statusTextView.visibility = View.GONE
        }
        SearchQueryStatus.EMPTY -> {
            statusTextView.visibility = View.VISIBLE
            statusTextView.text = statusTextView.context.getString(R.string.status_empty)
        }
        else -> {
            statusTextView.visibility = View.VISIBLE
            statusTextView.text = statusTextView.context.getString(R.string.status_unknown)
        }
    }
}