package com.shriakhilc.musicsearch.network

import com.squareup.moshi.Json

data class MusicTrack(
    @Json(name = "trackId") val trackId: Long,
    @Json(name = "artistName") val artistName: String,
    @Json(name = "collectionName") val albumName: String,
    @Json(name = "trackName") val trackName: String,
    @Json(name = "artworkUrl60") val albumImgUrl: String
)
