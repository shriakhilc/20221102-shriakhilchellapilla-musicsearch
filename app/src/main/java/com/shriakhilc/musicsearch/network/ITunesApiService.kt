package com.shriakhilc.musicsearch.network

import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

private const val BASE_URL = "https://itunes.apple.com"

private val moshi = Moshi.Builder()
    .add(KotlinJsonAdapterFactory())
    .build()

private val retrofit = Retrofit.Builder()
    .baseUrl(BASE_URL)
    .addConverterFactory(MoshiConverterFactory.create(moshi))
    .build()

interface ITunesApiService {
    // Passing default API limit and media type to filter only music
    @GET("search?limit=50&media=music")
    suspend fun getSearchResults(@Query("term") term: String): ITunesSearchResult
}

object ITunesApi {
    val retrofitService: ITunesApiService by lazy {
        retrofit.create(ITunesApiService::class.java)
    }
}