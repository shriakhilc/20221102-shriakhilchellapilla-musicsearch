package com.shriakhilc.musicsearch.network

import com.squareup.moshi.Json

data class ITunesSearchResult(
    @Json(name = "resultCount") val resultCount: Int,
    @Json(name = "results") val results: List<MusicTrack>,
)
