package com.shriakhilc.musicsearch.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.shriakhilc.musicsearch.databinding.ItemSearchResultBinding
import com.shriakhilc.musicsearch.network.MusicTrack

class MusicSearchAdapter :
    ListAdapter<MusicTrack, MusicSearchAdapter.TrackViewHolder>(DiffCallback) {

    class TrackViewHolder(
        private var binding: ItemSearchResultBinding
    ) : RecyclerView.ViewHolder(binding.root) {
        fun bind(musicTrack: MusicTrack) {
            binding.track = musicTrack
            binding.executePendingBindings()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TrackViewHolder {
        return TrackViewHolder(
            ItemSearchResultBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun onBindViewHolder(holder: TrackViewHolder, position: Int) {
        holder.bind(getItem(position))
    }


    companion object DiffCallback : DiffUtil.ItemCallback<MusicTrack>() {
        override fun areItemsTheSame(oldItem: MusicTrack, newItem: MusicTrack): Boolean {
            return oldItem.trackId == newItem.trackId
        }

        override fun areContentsTheSame(oldItem: MusicTrack, newItem: MusicTrack): Boolean {
            return oldItem == newItem
        }
    }

}