package com.shriakhilc.musicsearch.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.shriakhilc.musicsearch.network.ITunesApi
import com.shriakhilc.musicsearch.network.MusicTrack
import kotlinx.coroutines.launch

enum class SearchQueryStatus { LOADING, ERROR, DONE, EMPTY }

class MusicSearchViewModel : ViewModel() {
    private val _status = MutableLiveData<SearchQueryStatus>()
    val status: LiveData<SearchQueryStatus> = _status

    private val _results = MutableLiveData<List<MusicTrack>>()
    val results: LiveData<List<MusicTrack>> = _results

    init {
        // Loading app with sample query on startup
        getSearchResults("Tom Waits")
    }

    fun getSearchResults(query: String) {
        viewModelScope.launch {
            _status.value = SearchQueryStatus.LOADING
            try {
                val response = ITunesApi.retrofitService.getSearchResults(term = query)
                _results.value = response.results
                if (response.resultCount > 0) {
                    _status.value = SearchQueryStatus.DONE
                } else {
                    _status.value = SearchQueryStatus.EMPTY
                }
            } catch (e: Exception) {
                //Log.e("getSearchResults", "getSearchResults: ${e.message}")
                _status.value = SearchQueryStatus.ERROR
            }
        }
    }
}